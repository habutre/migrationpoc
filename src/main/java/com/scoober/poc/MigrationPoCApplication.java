package com.scoober.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MigrationPoCApplication {

    public static void main(String[] args) {
        SpringApplication.run(MigrationPoCApplication.class, args);
    }
}
