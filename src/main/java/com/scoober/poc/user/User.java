package com.scoober.poc.user;

import com.scoober.poc.base.MongoBase;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class User extends MongoBase {

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActve(boolean active) {
        this.active = active;
    }
}
