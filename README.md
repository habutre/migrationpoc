# Migration PoC

This little poc aims to present two concepts to software projects using persistence (you know any which doesn't uses one?)

* Simple auditing changes
* Migration tracking

## Auditing changes

Its not complex and complete auditing itself but basically record the creation and update times. With only theses records is already a good support for throubleshoot and realize weird or misbehavior.

The implementation uses a `BaseMongo` class which holds the common configuration for `entitiy` classes.


## Migration tracking

The migration here is targeting a `mongo` database. But if `mongo` is a schemaless database why creating a migration tracking process?

A schemaless doesn't means no-control, but specially flexibility.

You can see a example of migration to create a new collection, what initially can seems weird, can help to understand when a specific collection was introduced to the system.
For adding new keys, usually one have to insert a default or neutral value which will not change the current behavior of the current version running, but will guide the system to handle the new keys.

### The migration tool

The lib choosen was MongoTrek which uses a migration file in yaml format and allow native `mongo` command giving the flexibility and power of `mongo` commands not only related to CRUD operations.
If some system is dealing with millions, billions of records maybe apply a key update can cause unexpected behavior or even a donwtime of such system, slowing customer response and so on.


> Feel free to clone, make comments, open issues or simply blame about this initiative, but always bring a good approach or idea to improve this.
