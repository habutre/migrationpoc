package com.scoober.poc;

import net.ozwolf.mongo.migrations.MongoTrek;
import net.ozwolf.mongo.migrations.exception.MongoTrekFailureException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoAuditing
@EnableMongoRepositories
class MongoConfig {

    @Value("${migrationpoc.migrationfile}")
    private String migrationFile;

    @Value("${spring.data.mongodb.host}")
    private String mongoHost;

    @Value("${spring.data.mongodb.port}")
    private Integer mongoPort;

    @Value("${spring.data.mongodb.database}")
    private String mongoDatabaseName;

    @Bean
    public Boolean runMigrations() throws MongoTrekFailureException {

        String embeddedMongoUrl = String.format("mongodb://%s:%s/%s", mongoHost, mongoPort, mongoDatabaseName);

        MongoTrek migrationEngine = new MongoTrek(migrationFile, embeddedMongoUrl);

        migrationEngine.migrate();

        return true;
    }
}