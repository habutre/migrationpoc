package com.scoober.poc.user;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Autowired
    private UserService service;
    @Autowired
    private UserRepository repository;

    @After
    public void cleanUp() {
        repository.deleteAll();
    }

    @Test
    public void givenAEmptyUsersCollection_whenNoFilterIsInformed_thenExpectZeroUsersRetrieved() {
        final List<User> users = service.all();

        assertThat(users.size()).isEqualTo(0);
    }

    @Test
    public void givenAEmptyUsersCollection_whenAUserIsCreatedWithoutAnyField_thenExpectOneUserBeRetrievedWithDefaultFieldsFilled() {
        User user = service.create(new User());

        assertThat(user).isNotNull();
        assertThat(user.getId()).isNotBlank();
        assertThat(user.getId()).inHexadecimal();
        assertThat(user.getCreatedAt()).isToday();
        assertThat(user.getUpdatedAt()).isToday();
    }

    @Test
    public void givenANonEmptyUsersCollection_whenNoFiltersAreApplied_thenExpectAllUsersBeRetrieved() {
        service.create(new User());

        final List<User> users = service.all();

        assertThat(users.size()).isGreaterThan(0);
    }

    @Test
    public void givenAExistentUser_whenThatUserIsChanged_thenExpectUpdatedDateBeAfterCreatedDate() {
        service.create(new User());
        final List<User> users = service.all();

        assertThat(users.size()).isEqualTo(1);
        assertThat(users.get(0).getCreatedAt()).isEqualTo(users.get(0).getUpdatedAt());

        Date createdAt = users.get(0).getCreatedAt();
        service.update(users.get(0));

        assertThat(users.size()).isEqualTo(1);
        assertThat(users.get(0).getCreatedAt()).isEqualTo(createdAt);
        assertThat(users.get(0).getUpdatedAt()).isAfter(users.get(0).getCreatedAt());
    }
}